function fact(n) {
    var ret = 1;
    if(n > 0)
	ret = n * fact(n - 1);
    return ret;
}

console.log(fact(5));
